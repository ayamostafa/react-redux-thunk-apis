import React from "react";
import { connect } from "react-redux";

//import { fetchUser } from "../actions";
class UserHeader extends React.Component {
    componentDidMount() {
        //      this.props.fetchUser(this.props.id);
    }
    render() {
        //const user = this.props.users.find(user => user.id === this.props.id)
        // console.log(user);
        if (!this.props.user) return null;
        return <div><b>{this.props.user.name}</b></div>
    }
}

const mapStateToProps = (state, ownProps) => {
    return { user: state.users.find(user => user.id === ownProps.id) };
}
//export default connect(mapStateToProps, { fetchUser })(UserHeader);
export default connect(mapStateToProps)(UserHeader);