import React from 'react';
import { connect } from "react-redux";
//First attempt with _.memoize
//import { fetchPosts } from '../actions';

//Second Attempt
import { fetchPostsAndUsers } from '../actions';

import UserHeader from "./UserHeader";
class PostList extends React.Component {
    componentDidMount() {
        //for first _.memozie
        //  this.props.fetchPosts();

        this.props.fetchPostsAndUsers();
    }
    renderList() {
        return this.props.posts.map(post => {
            return (
                <div key={post.id}>
                    <h1>{post.title}</h1>
                    <p>{post.body}</p>
                    <UserHeader id={post.userId} />
                    <hr />
                </div>

            )
        })
    }
    render() {
        // console.log(this.props.posts)
        return <div>{this.renderList()}</div>;

    }
}

const mapStateToProps = (state) => {
    return { posts: state.posts }
}
//for first sol
//export default connect(mapStateToProps, { fetchPosts })(PostList);

//for second sol
export default connect(
    mapStateToProps,
    { fetchPostsAndUsers }
)(PostList);


// export default connect(
//     null,
//     { fetchPosts }
// )(PostList);
