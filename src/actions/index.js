import jsonPlaceholder from '../apis/jsonPlaceholder';
import _ from "lodash";


//Solution2 to fetch user data based on posts with unique user ids then call user api to each unique userID
export const fetchPostsAndUsers = () => async (dispatch, getState) => {
    await dispatch(fetchPosts());

    const userIds = _.uniq(_.map(getState().posts, 'userId'));
    userIds.forEach(id => dispatch(fetchUser(id)));
};


export const fetchPosts = () => async dispatch => {
    const response = await jsonPlaceholder.get('/posts');

    dispatch({ type: 'FETCH_POSTS', payload: response.data });
};

//Solution1 with _.memoize to fetch user data once
// export const fetchUser = (id) => (dispatch) => {
//     _fetchUser(id, dispatch);
// }
// const _fetchUser = _.memoize(async function (id, dispatch) {
//     const response = await jsonPlaceholder.get(`/users/${id}`);

//     dispatch({ type: 'FETCH_USER', payload: response.data });
// });


export const fetchUser = id => async dispatch => {
    const response = await jsonPlaceholder.get(`/users/${id}`);

    dispatch({ type: 'FETCH_USER', payload: response.data });
};


// import jsonPlaceholder from "../apis/jsonPlaceholder";

// export const fetchPosts = () => {
//     return async function (dispatch, getState) {
//         const response = await jsonPlaceholder.get('/posts');
//         dispatch({
//             type: 'Fetch_Data',
//             payload: response
//         });
//     }
// }